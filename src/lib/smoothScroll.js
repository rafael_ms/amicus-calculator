const smoothScroll = (item, forward = false, callback, context = window, start = window.pageYOffset, duration = 600) => {
    const headerMenu = document.querySelector('#menu-2017')
    const headerHeight = headerMenu ? getComputedStyle(headerMenu).height : 0
    const end = forward ? item.offsetTop - parseInt(headerHeight, 10) : (window.pageYOffset - item.getBoundingClientRect().bottom - window.innerHeight) + 100
    const clock = Date.now()
    const requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || ((fn) => window.setTimeout(fn, 15))
    const easeInOutCubic = (t) => (t < 0.5 ? 4 * t * t * t : ((t - 1) * ((2 * t) - 2) * ((2 * t) - 2)) + 1)

    const position = (startP, endP, elapsed) => {
        if (elapsed > duration) return endP
        return startP + ((endP - startP) * easeInOutCubic(elapsed / duration))
    }

    const step = () => {
        const elapsed = Date.now() - clock
        if (context !== window) {
            context.scrollTop = position(start, end, elapsed)
        } else {
            window.scroll(start, position(start, end, elapsed))
        }

        if (elapsed > duration) {
            if (callback) {
                callback()
            }
        } else {
            requestAnimationFrame(step)
        }
    }

    step()
}

export default smoothScroll
