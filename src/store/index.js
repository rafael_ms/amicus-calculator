import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// common state
const state = {
    form: {
        firstName: '',
        lastName: '',
        phone: '',
        state: '',
        company: '',
        email: '',
        subscribe_updates: false
    }
}

 // common actions
const actions = {}

// common getters
const getters = {
    form: (state) => state.form
}

// common mutations
const mutations = {
    setFormField: (state, { field, val }) => {
        state.form[field] = val
    }
}

export default new Vuex.Store({
    state,
    actions,
    getters,
    mutations,
    strict: process.env.NODE_ENV !== 'production'
})
